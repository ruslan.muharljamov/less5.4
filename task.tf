variable "instance_count" {
  default = "2"
}

#disks
resource "google_compute_disk" "disk1" {
  count = var.instance_count
  name  = "instance${count.index}-disk10"
  type  = "pd-standard"
  size  = 10
  zone  = "europe-central2-a"
}

resource "google_compute_disk" "disk2" {
  count = var.instance_count
  name  = "instance${count.index}-disk8"
  type  = "pd-standard"
  size  = 8
  zone  = "europe-central2-a"
}

#instance
#resource "google_compute_address" "my-ip" {
#  name = "ip-address"
#}

data "google_compute_image" "debian_image" {
  family  = "debian-10"
  project = "debian-cloud"
}

resource "google_compute_instance" "vm-instance" {
  count        = var.instance_count
  name         = "vm-instance${count.index}"
  machine_type = "e2-small"
  network_interface {
    network = "default"
    access_config {
      #    nat_ip = google_compute_address.my-ip.address
    }
  }
  boot_disk {
    initialize_params {
      image = data.google_compute_image.debian_image.self_link
    }
  }

  #atach disks
  #attached_disk {
  #  source = "disk10"
  #}
  #attached_disk {
  #    source = "disk8"
  #  }
  #nginx on docker
  connection {
    user        = "tatflyruskey"
    private_key = file(var.private_key_path)
    timeout     = "2m"
    host        = self.network_interface.0.access_config.0.nat_ip
  }

  provisioner "remote-exec" {
    inline = ["sudo apt update",
      "sudo apt install -y apt-transport-https ca-certificates curl gnupg lsb-release",
      "curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg",
      "echo \"deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable\" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null",
      "sudo apt update",
      "sudo apt install -y docker-ce docker-ce-cli containerd.io",
      "sudo usermod -a -G docker tatflyruskey",
      "sudo sh -c 'echo Juneway ${self.network_interface.0.access_config.0.nat_ip} ${self.name} > /root/index.html'",
      "sudo docker run --name enginx -p 80:80 -v /root/index.html:/usr/share/nginx/html/index.html -d nginx"
    ]

  }
}
#idon'tfuckinremembermynicontelega
#resource "cloudflare_record" "juneway" {
#  zone_id = var.zone_id
#  name    = "El_n3gro.juneway.pro"
#  value   = google_compute_forwarding_rule.forwarding_rule.ip_address
#  type    = "A"
#  ttl     = 3600
#}
#atach disks
resource "google_compute_attached_disk" "disk1" {
  count    = var.instance_count
  disk     = element(google_compute_disk.disk1.*.self_link, count.index)
  instance = element(google_compute_instance.vm-instance.*.self_link, count.index)
}
resource "google_compute_attached_disk" "disk2" {
  count    = var.instance_count
  disk     = element(google_compute_disk.disk2.*.self_link, count.index)
  instance = element(google_compute_instance.vm-instance.*.self_link, count.index)
}

#fraer_wall
resource "google_compute_firewall" "ports" {
  name    = "opened-ports"
  network = "default"

  dynamic "allow" {
    for_each = ["80"]
    content {
      protocol = "tcp"
      ports    = ["${allow.value}"]
    }
  }

  source_ranges = ["0.0.0.0/0"]
}

#allow access from IAP and health check
resource "google_compute_firewall" "fw-iap" {
  name          = "fw-iap"
  network       = "default"
  source_ranges = ["130.211.0.0/22", "35.191.0.0/16", "35.235.240.0/20"]
  allow {
    protocol = "tcp"
  }
}

# allow from proxy to backends
resource "google_compute_firewall" "fw-lb-to-backends" {
  name          = "fw-lb-to-backends"
  network       = "default"
  source_ranges = ["10.0.0.0/24", "10.186.0.0/24"]
  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
}


#proxys and other gomosyatina
resource "google_compute_instance_group" "nginx_group" {
  name      = "nginx-instance-group"
  zone      = "europe-central2-a"
  instances = tolist(google_compute_instance.vm-instance.*.id)
  named_port {
    name = "http"
    port = "80"
  }
}

resource "google_compute_backend_service" "nginx_service" {
  name      = "nginx-service"
  port_name = "http"
  protocol  = "HTTP"

  backend {
    group = google_compute_instance_group.nginx_group.id
  }

  health_checks = [
    google_compute_http_health_check.nginx_health.id,
  ]
}

resource "google_compute_http_health_check" "nginx_health" {
  name         = "nginx-health"
  request_path = "/"
  port         = 80
}

resource "google_compute_target_http_proxy" "default" {
  name    = "proxy"
  url_map = google_compute_url_map.default.id
}

resource "google_compute_url_map" "default" {
  name            = "url-map"
  default_service = google_compute_backend_service.nginx_service.id
}


# forwarding rule
resource "google_compute_global_forwarding_rule" "forwarding_rule" {
  name        = "forwarding-rule"
  ip_protocol = "TCP"
  port_range  = "80"
  load_balancing_scheme = "EXTERNAL"
  target      = google_compute_target_http_proxy.default.id
}
