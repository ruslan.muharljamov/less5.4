provider "google" {
  region  = "europe-central2"
  zone    = "europe-central2-a"
  project = "steady-freedom-328709"
}

terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 2.0"
    }
  }
}
provider "cloudflare" {
  email      = var.email
  api_key    = var.api_key
  account_id = var.account_id
}
